<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// Route::get('/', function () {
	//INITIAL MIGRATION
	// Excel::batch('public/csvs', function ($rows, $files) {
	// 	$rows->each(function ($row) {
	// 		Temp::create($row->toArray());
	// 	});
	// });
// });

Route::get('/', array('as' => 'home', 'uses' => 'AppController@getIndex'));
Route::get('/tax', array('as' => 'tax', 'uses' => 'AppController@getTax'));
Route::post('/tax', array('as' => 'post_tax', 'uses' => 'AppController@postTax'));
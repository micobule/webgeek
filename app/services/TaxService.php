<?php

class TaxService
{
	public function __construct(
		Agencies $agenciesModel,
		Departments $departmentsModel,
		Efforts $effortsModel
	)
	{
		$this->agenciesModel = $agenciesModel;
		$this->departmentsModel = $departmentsModel;
		$this->effortsModel = $effortsModel;
	}

	public function compute($input)
	{
		$tax = $this->computeTax(array_pull($input, 'taxable'));
		$results = $this->computeEfforts($tax, array_pull($input, 'efforts'));

		return array(
			'tax' => $tax,
			'efforts' => $results
		);
	}

	private function computeTax($taxable)
	{
		$tax = 0;

		if ($taxable >= 4167 && $taxable < 5000) {
			$tax = ($taxable - 4167) * 0.05;
		}

		if ($taxable >= 5000 && $taxable < 6667) {
			$tax = 41.67 + (($taxable - 5000) * 0.1);
		}

		if ($taxable >= 6667 && $taxable < 10000) {
			$tax = 208.33 + (($taxable - 6667) * 0.15);
		}

		if ($taxable >= 10000 && $taxable < 15833) {
			$tax = 708.33 + (($taxable - 10000) * 0.2);
		}

		if ($taxable >= 15833 && $taxable < 25000) {
			$tax = 1875 + (($taxable - 15833) * 0.25);
		}

		if ($taxable >= 25000 && $taxable < 45833) {
			$tax = 4166.67 + (($taxable - 25000) * 0.30);
		}

		if ($taxable >= 45833) {
			$tax = 5208.33 + (($taxable - 45833) * 0.32);
		}

		return $tax;
	}

	private function computeEfforts($tax, $supportedEfforts)
	{
		$results = array();
		$totalBudget = 0;

		$efforts = $this->effortsModel->all();

		foreach ($efforts as $effort) {
			foreach ($effort->agencies as $agency) {
				$totalBudget += $agency->pivot->amount;
			}
		}

		foreach ($efforts as $effort) {
			$effortBudget = 0;

			foreach ($effort->agencies as $agency) {
				$effortBudget += $agency->pivot->amount;
			}

			$effort->budget = $effortBudget;
			$effort->percentage = $effortBudget/$totalBudget;
		}

		foreach ($efforts as $effort) {
			foreach ($supportedEfforts as $supportedEffort) {
				if ($effort->id == $supportedEffort) {
					$effort->contribution = $tax * $effort->percentage;
					array_push($results, $effort);
				}
			}
		}

		return $results;
	}
}
<?php

class Departments extends Eloquent {
	protected $table = 'departments';
	protected $fillable = array('description');
	protected $guarded = array('id');

	public function agencies()
	{
		return $this->hasMany('Agencies');
	}
}
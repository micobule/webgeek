<?php

class Efforts extends Eloquent
{
	protected $table = 'efforts';
	protected $fillable = array('description');
	protected $guarded = array('id');
	protected $appends = array('budget', 'percentage', 'contribution');

	public function agencies()
	{
		return $this->belongsToMany('Agencies', 'records')->withPivot('amount');
	}

	public function getBudgetAttribute()
	{
		return $this->attributes['budget'];
	}

	public function getPercentageAttribute()
	{
		return $this->attributes['percentage'];
	}

	public function getContributionAttribute()
	{
		return $this->attributes['contribution'];
	}
}
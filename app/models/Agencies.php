<?php

class Agencies extends Eloquent {
	protected $table = 'agencies';
	protected $fillable = array('description', 'code', 'departments_id');
	protected $guarded = array('id');

	public function department()
	{
		return $this->belongsTo('Departments');
	}

	public function efforts()
	{
		return $this->belongsToMany('Efforts', 'records')->withPivot('amount');
	}
}
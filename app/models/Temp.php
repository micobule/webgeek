<?php

class Temp extends Eloquent {
	protected $table = 'temp';
	protected $fillable = array('uacs_dpt_id','uacs_dpt_dsc','uacs_agy_id','uacs_agy_dsc','uacs_fpap_id','uacs_fpap_dsc','operdiv','operunit','uacs_oper_dsc', 'fndsrc','uacs_fund_subcat_dsc','uacs_exp_cd','uacs_exp_dsc','uacs_sobj_cd','uacs_sobj_dsc','amt');
}
<!DOCTYPE html>
<html>
	<head>
		@include('common/head')
	</head>
	<body>
		@include ('common/navbar')
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-4 col-md-offset-4">
					@if (Session::get('error'))
						@include('common/error')
					@endif
				</div>
			</div>
			@yield('content')
		</div>
		<div class="bg-black-50">
		</div>
		@include('common/default_scripts')
		@yield('scripts')
	</body>
</html>
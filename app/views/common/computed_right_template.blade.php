<br>
<div class="row">
	<div class="col-md-3">
		<a href="#" class="thumbnail">
			<img src="{{ asset($effort->image_links) }}"/>
	  </a>
	</div>
	<div class="col-md-9">
		<h2>{{ $effort->description }}</h2>
		<p>You contributed {{ round($effort->percentage, 4) }}% of your tax (Php {{ round($effort->contribution, 2) }}) to this effort which has a total budget of Php {{ round($effort->budget, 2) }}</p>
	</div>
</div>
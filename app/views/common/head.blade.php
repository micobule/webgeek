<link rel="stylesheet" href="packages/bootstrap/dist/css/bootstrap-theme.css"/>
<link rel="stylesheet" href="packages/bootstrap/dist/css/bootstrap.min.css"/>
<link rel="stylesheet" href="packages/fontawesome/css/font-awesome.min.css"/>
<link rel="stylesheet" href="site/main.css"/>
<script>
	function select(element) {
		var id = $(element).attr('data-id');
		
		if(typeof($('#pic-' + id).attr('name')) !== 'undefined' || $('#pic-' + id).attr('name') == '') {
			$('#pic-' + id).attr('name', '');
			$(element).addClass('filtered');
		} else {
			$('#pic-' + id).attr('name', 'efforts[]');
			$(element).removeClass('filtered');
		}
	}
</script>
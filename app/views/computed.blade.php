@extends('common/template')

@section('content')
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<h2>Your contribution of Php {{ $results['tax'] }} are divided to the ff:</h2>
			@foreach ($results['efforts'] as $effort)
				@include('common/computed_right_template', array('effort' => $effort))
			@endforeach
		</div>
	</div>
	<div class="row">
		<div class="col-md-5 col-md-offset-1">
			<h2>How do you feel about that?</h2>
			<div class="row">
				<div class="col-md-3">
			    <a class="thumbnail">
			      <img src="{{ asset('/site/images/logo-up.png') }}">
			    </a>
			  </div>
			  <div class="col-md-3">
			    <a class="thumbnail">
			      <img src="{{ asset('/site/images/logo-down.png') }}">
			    </a>
			  </div>
			</div>
		</div>
		<div class="col-md-5 col-md-offset-1">
			<h2>Share your results</h2>
			<div class="row">
				<div class="col-md-3">
					<a href="#"><i class="fa fa-facebook fa-5x"></i></a>
				</div>
			  <div class="col-md-3">
			    <a href="#"><i class="fa fa-twitter fa-5x"></i></a>
			  </div>
			</div>
		</div>
	</div>
@stop
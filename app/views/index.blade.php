@extends('common/template')

@section('content')
	<div class="row">
		<div class="col-md-12">
			<div class="row">
				<div class="col-md-3">
					<h1 class="text-center">Which government efforts do you support?</h1>
				</div>
				@for ($x = 0; $x < count($efforts); $x++)
				  <div class="col-md-3">
				    <a class="thumbnail">
				      <img src="{{ asset($efforts[$x]->image_links) }}" data-id="{{ $efforts[$x]->id }}" onclick="select(this)" class="filtered">
				    </a>
				  </div>
				  @if ($x == 2)
				  	<span class="clearfix"></span>
				  @endif
				@endfor
			</div>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-md-6">
			<p class="lead"><strong>TAXES</strong> are a burden. You feel like your money is being stolen. Click <strong>Go!</strong> and let <strong>ThumbTax HELP YOU FEEL GOOD</strong> about your tax!</p>
		</div>
		<div class="col-md-4 col-md-offset-1">
			{{ Form::open(array('url' => route('post_tax'))) }}
				@foreach ($efforts as $effort)
					<input type="hidden" value="{{ $effort->id }}" id="pic-{{ $effort->id }}"/>
				@endforeach
				<div class="input-group">
					<span class="input-group-addon">Php</span>
					<input class="form-control" type="text" placeholder="Taxable income" name="taxable">
				</div>
				<br>
				<input class="btn btn-primary btn-block" type="submit" value="Go!">
			{{ Form::close() }}
		</div>
	</div>
@stop
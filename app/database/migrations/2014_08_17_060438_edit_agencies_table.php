<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditAgenciesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('agencies', function($table) {
			$table->renameColumn('agency_id', 'id');
			$table->renameColumn('agency_dsc', 'description');
			$table->renameColumn('agency_code', 'code');
			$table->renameColumn('department_id', 'departments_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('agencies', function ($table) {
			$table->renameColumn('id', 'agency_id');
			$table->renameColumn('description', 'agency_dsc');
			$table->renameColumn('code', 'agency_code');
			$table->renameColumn('departments_id', 'department_id');
		});
	}

}

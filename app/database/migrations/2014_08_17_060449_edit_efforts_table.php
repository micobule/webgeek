<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditEffortsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('efforts', function ($table) {
			$table->renameColumn('effort_id', 'id');
			$table->renameColumn('effort_dsc', 'description');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('efforts', function ($table) {
			$table->renameColumn('id', 'effort_id');
			$table->renameColumn('description', 'effort_dsc');
		});
	}

}

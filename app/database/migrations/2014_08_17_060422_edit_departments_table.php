<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditDepartmentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('departments', function ($table) {
			$table->renameColumn('department_id', 'id');
			$table->renameColumn('department_dsc', 'description');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		$table->renameColumn('id', 'department_id');
		$table->renameColumn('description', 'department_dsc');
	}

}

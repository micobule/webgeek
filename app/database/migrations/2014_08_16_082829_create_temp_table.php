<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTempTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('temp', function($table) {
			$table->integer('uacs_dpt_id')->nullable();
			$table->string('uacs_dpt_dsc')->nullable();
			$table->integer('uacs_agy_id')->nullable();
			$table->string('uacs_agy_dsc')->nullable();
			$table->integer('uacs_fpap_id')->nullable();
			$table->string('uacs_fpap_dsc')->nullable();
			$table->string('operdiv')->nullable();
			$table->integer('operunit')->nullable();
			$table->string('uacs_oper_dsc')->nullable();
			$table->integer('fndsrc')->nullable();
			$table->string('uacs_fund_subcat_dsc')->nullable();
			$table->integer('uacs_exp_cd')->nullable();
			$table->string('uacs_exp_dsc')->nullable();
			$table->integer('uacs_sobj_cd')->nullable();
			$table->string('uacs_sobj_dsc')->nullable();
			$table->integer('amt')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('temp'); 
	}

}

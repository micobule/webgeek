<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditRecordsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('records', function ($table) {
			$table->renameColumn('age_id', 'agencies_id');
			$table->renameColumn('eff_id', 'efforts_id');
			$table->renameColumn('amt', 'amount');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('records', function ($table) {
			$table->renameColumn('agencies_id', 'age_id');
			$table->renameColumn('efforts_id', 'eff_id');
			$table->renameColumn('amount', 'amt');
		});
	}

}

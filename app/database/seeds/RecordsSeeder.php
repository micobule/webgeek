<?php

class RecordsSeeder extends Seeder
{
	public function run()
	{
		$results = DB::select('SELECT department_id, agency_id, effort_code, sum(amt) amt FROM source GROUP BY department_id, agency_id, effort_code');

		foreach ($results as $result) {
			$array = array(
				'age_id' => $result->agency_id,
				'eff_id' => $result->effort_code,
				'amt' => $result->amt
			);

			DB::table('records')->insert($array);
		}
	}
}
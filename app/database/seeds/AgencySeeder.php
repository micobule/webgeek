<?php

class AgencySeeder extends Seeder
{
	public function run()
	{
		$agencies = DB::select('SELECT distinct `uacs_agy_id`, `uacs_agy_dsc`, `uacs_dpt_id` FROM `temp`');

		foreach ($agencies as $agency) {
			if ($agency->uacs_agy_dsc != null) {
				$array = array(
					'agency_code' => $agency->uacs_agy_id,
					'agency_dsc' => $agency->uacs_agy_dsc,
					'department_id' => $agency->uacs_dpt_id
				);

				Agencies::create($array);
			}
		}
	}
}
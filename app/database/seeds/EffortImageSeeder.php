<?php

class EffortImageSeeder extends Seeder
{
	public function run()
	{
		$effort = Efforts::find(1);
		$effort->image_links = '/site/images/BuildingsAndOtherStructures.jpg';
		$effort->save();
		$effort = Efforts::find(5);
		$effort->image_links = '/site/images/RoadsAndBridges.jpg';
		$effort->save();
		$effort = Efforts::find(8);
		$effort->image_links = '/site/images/Education.jpg';
		$effort->save();
		$effort = Efforts::find(9);
		$effort->image_links = '/site/images/EnvironmentProtection.jpg';
		$effort->save();
		$effort = Efforts::find(11);
		$effort->image_links = '/site/images/Health.jpg';
		$effort->save();
		$effort = Efforts::find(12);
		$effort->image_links = '/site/images/Sports.jpg';
		$effort->save();
		$effort = Efforts::find(13);
		$effort->image_links = '/site/images/ResearchAndDev.jpg';
		$effort->save();
	}
}
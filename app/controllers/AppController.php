<?php

class AppController extends BaseController
{
	public function __construct(
		TaxService $taxService,
		Efforts $effortsModel
	)
	{
		$this->taxService = $taxService;
		$this->effortsModel = $effortsModel;
	}

	public function getIndex()
	{
		$efforts = $this->effortsModel->all();
		return View::make('index')->with('efforts', $efforts);
	}

	public function getTax()
	{
		return View::make('computed');
	}

	public function postTax()
	{
		$validator = Validator::make(Input::except('_token'), array('taxable' => 'required', 'efforts' => 'required'));

		if ($validator->fails()) {
			return Redirect::to('/')->withErrors($validator);
		}

		$results = $this->taxService->compute(Input::except('_token'));

		return View::make('computed')->with('results', $results);
	}
}